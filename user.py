try:
    import bcrypt
except ImportError:
    print("\nError! Necessary libraries unavailable...\n")


class User:
    IsAuthenticated = False
    Username = ""
    __Attempts = 0

    def __init__(self, username_input, password_input):

        # Initialise private variables
        self.__username_input = username_input
        self.__password_input = password_input

    def SignUp(self):
        '''Takes the username and password used to initialise the object, and stores them securely'''

        username = self.__username_input
        # Encode password to UTF-8 for use by BCrypt
        password_bytes = bytes(self.__password_input, "UTF-8")
        # Hash password using the BCrypt algorithm
        hashed_password = bcrypt.hashpw(password_bytes, bcrypt.gensalt(rounds=14))

        username_available = False  # True for testing - change to False for

        # Checks if the username is available
        try:
            # db[username]
        except KeyError:
            username_available = True

        if username_available:
            # Store password in database
            # db[username] = hashed_password.decode()
            self.Username = username
            self.IsAuthenticated = True
            return True
        else:
            return False

    def Authenticate(self):

        # Default to False
        authenticated = False
        username = self.__username_input
        # Encode password to UTF-8 for use by BCrypt
        password_bytes = self.__password_input.encode("UTF-8")

        # Checks if the username is recognised
        try:
            stored_password = # db[username]
        except KeyError:
            return False

        # Will return True if the inputted password matches the stored hash
        authenticated = bcrypt.checkpw(password_bytes, stored_password.encode("UTF-8"))

        if authenticated:
            self.IsAuthenticated = True
            self.Username = username
            return True
        else:
            self.__Attempts += 1
            return False

    def RemoveUser(self):

        username = self.__username_input

        try:
            # Credentials can only be removed if the user has logged in
            if self.IsAuthenticated == True:
                del # db[username]
                self.IsAuthenticated = False
                return True
            else:
                return False
        except:
            return False
