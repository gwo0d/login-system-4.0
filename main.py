try:
    from user import User
    import os
    import time
except ImportError:
    print("\nError! Necessary libraries unavailable...\n")


def clear():
    time.sleep(2.5)
    os.system('cls' if os.name == 'nt' else 'clear')


username = input("Username: ")
password = input("Password: ")

# Example Username: testUser
# Example Password: testPass

user = User(username, password)

go = True

while go:
    try:
        menu = int(input(
            "\nEnter 1 to login\nEnter 2 to sign up\nEnter 3 to delete your credentials from the database\nEnter 4 to exit\n: "))

        if menu == 1:

            result = user.Authenticate()

            if result == True:
                print(f"\nSuccess!\nYou are logged in as: {user.Username}")
                clear()
            else:
                print("\nError! please try again...")
                clear()

        elif menu == 2:

            result = user.SignUp()

            if result == True:
                print(f"\nSuccess!\n\nYou are logged in as: {user.Username}")
                clear()
            else:
                print("\nError! that username is taken...")
                clear()

        elif menu == 3:

            result = user.RemoveUser()

            if result == True:
                print(f"\nThe credentials associated with the username {username} have been deleted.")
                clear()
                # Terminate program as User object must be recreated.
                go = False
            else:
                print("\nPlease make sure you are logged in before deleting your credentials.\n")
                clear()

        elif menu == 4:
            clear()
            go = False

        else:
            print("\nError! Please try again...")
            clear()
    except:
        print("\nAn unexpected error has occurred! Please try again...")
        clear()
